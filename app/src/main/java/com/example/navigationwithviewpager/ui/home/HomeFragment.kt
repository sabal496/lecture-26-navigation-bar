package com.example.navigationwithviewpager.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.navigationwithviewpager.R
import com.example.navigationwithviewpager.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : BaseFragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
     }

    override fun getresource(): Int =R.layout.fragment_home
/*    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        root = inflater.inflate(R.layout.fragment_home, container, false)
        init()
        return root
    }*/

  private fun  init(){
      homeViewModel =
          ViewModelProvider(this).get(HomeViewModel::class.java)
      homeViewModel.initcount()

      homeViewModel.getitemcount().observe(viewLifecycleOwner, Observer {
          root!!.text_home.text = it.toString()
      })
      root?.plusbtn?.setOnClickListener(){
          homeViewModel.plus()
      }

      root?.minusbtn?.setOnClickListener(){
          homeViewModel.minus()
      }
  }
}
