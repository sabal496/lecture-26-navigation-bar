package com.example.navigationwithviewpager.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class Viewpageradapter(fm : FragmentManager,private val fragments:MutableList<Fragment>):FragmentStatePagerAdapter(fm,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return  fragments[position]
     }

    override fun getCount(): Int =fragments.size
}