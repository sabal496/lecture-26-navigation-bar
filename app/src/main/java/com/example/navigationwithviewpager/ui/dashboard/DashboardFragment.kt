package com.example.navigationwithviewpager.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.navigationwithviewpager.R
import com.example.navigationwithviewpager.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_dashboard.*

class DashboardFragment :BaseFragment(){

    private lateinit var dashboardViewModel: DashboardViewModel

/*    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {


    }*/

    private fun init(){

        dashboardViewModel =
            ViewModelProvider(this).get(DashboardViewModel::class.java)
         dashboardViewModel.text.observe(viewLifecycleOwner, Observer {
             text_dashboard.text=it
        })
    }

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
     }

    override fun getresource(): Int=R.layout.fragment_dashboard

}
