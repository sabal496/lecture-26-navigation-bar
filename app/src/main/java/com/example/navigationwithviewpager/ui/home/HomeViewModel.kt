package com.example.navigationwithviewpager.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    private val count :MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }

    fun initcount(){
        count.value=0
    }
    fun plus(){
        count.value= count.value!!.plus(1)
    }
    fun minus(){
        count.value= count.value?.minus(1)
    }

    fun getitemcount():MutableLiveData<Int>{
        return  count
    }

    /*private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text*/
}