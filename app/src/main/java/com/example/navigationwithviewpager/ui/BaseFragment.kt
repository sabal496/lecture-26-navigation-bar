package com.example.navigationwithviewpager.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BaseFragment:Fragment() {

    var root:View?=null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if(root==null){
            root=inflater.inflate(getresource(), container, false)
            start(inflater,container,savedInstanceState)
        }

        return  root

    }

    abstract  fun start(inflater: LayoutInflater,
                        container: ViewGroup?,
                        savedInstanceState: Bundle?)

    abstract  fun getresource():Int

}