package com.example.navigationwithviewpager

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.viewpager.widget.ViewPager
import com.example.navigationwithviewpager.ui.Viewpageradapter
import com.example.navigationwithviewpager.ui.dashboard.DashboardFragment
import com.example.navigationwithviewpager.ui.home.HomeFragment
import com.example.navigationwithviewpager.ui.notifications.NotificationsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
       /* val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)*/
    }
    private fun init(){
        val fragments= mutableListOf<Fragment>()
        fragments.add(HomeFragment())
        fragments.add(DashboardFragment())
        fragments.add(NotificationsFragment())
        viewpager.adapter=Viewpageradapter(supportFragmentManager,fragments)

        viewpager.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

             }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
             }

            override fun onPageSelected(position: Int) {
                nav_view.menu.getItem(position).isChecked=true
             }
        })

        nav_view.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.navigation_home->viewpager.currentItem=0
                R.id.navigation_dashboard->viewpager.currentItem=1
                R.id.navigation_notifications->viewpager.currentItem=2
            }
            true
        }


    }
}
